using System.Threading.Tasks;
using ENode.Commanding;
using ENode.Infrastructure;
using ENodeMicro.Commands.Managers;
using ENodeMicro.Domain.Managers;

namespace ENodeMicro.CommandHandlers
{
    /// <summary>
    /// 经营者命令处理程序
    /// </summary>
    public class ManagerCommandHandler :
        ICommandHandler<CreateManagerCommand>
    {
        /// <summary>
        /// 创建经营者命令
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public async Task HandleAsync(ICommandContext context, CreateManagerCommand command)
        {
            await context.AddAsync(new Manager(command.AggregateRootId, command.Name, command.PhoneNumber));
        }
    }
}