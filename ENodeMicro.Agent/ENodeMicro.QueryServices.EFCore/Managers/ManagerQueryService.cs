using System.Linq;
using System.Threading.Tasks;
using ECommon.Components;
using ENodeMicro.Common;
using ENodeMicro.QueryServices.Interfaces.Managers;
using ENodeMicro.QueryServices.Models.Managers;
using Microsoft.EntityFrameworkCore;

namespace ENodeMicro.QueryServices.EFCore.Managers
{
    /// <summary>
    /// 经营者查询服务接口实现类
    /// </summary>
    [Component]
    public class ManagerQueryService : QueryServiceBase<Manager>, IManagerQueryService
    {
        public ManagerQueryService(AgentDbContext context) : base(context)
        {
        }

        public async Task<APIResult<Manager>> GetDetailAsync(string id)
        {
            return await QueryResultAsync(async (query) =>
            {
                var  rsp = new APIResult<Manager>();
                var res = await query.Where(p => p.Id == id).FirstOrDefaultAsync();
                if (res == null)
                {
                    rsp.Message = "暂无数据.";
                    return rsp;
                }
                rsp.Message = "读取成功";
                rsp.Data = res;
                rsp.Success = true;
                return rsp;
            });
        }
    }
}