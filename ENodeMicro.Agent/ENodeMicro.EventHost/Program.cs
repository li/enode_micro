﻿using System;
using ENodeMicro.Common;
using Microsoft.Extensions.Configuration;

namespace ENodeMicro.EventHost
{
    class Program
    {
        static void Main(string[] args)
        {
            Initialize();
            Bootstrap.Initialize();
            Bootstrap.Start();
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
        }

        /// <summary>
        /// 初始化
        /// </summary>
        static void Initialize()
        {
            // 读取环境变量
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            System.Console.WriteLine($"Environment:{env}");

            var builder = new ConfigurationBuilder()
                .AddJsonFile(env == "Development" ? "appsettings.Development.json" : "appsettings.json");

            var configuration = builder.Build();

            #region 配置数据库
            ConfigSettings.BusinessConnectionString = configuration.GetSection("ConnectionStrings")["business"];
            ConfigSettings.ENodeConnectionString = configuration.GetSection("ConnectionStrings")["enode"];
            #endregion
        }
    }
}