namespace ENodeMicro.WebAPI.Models.Members
{
    /// <summary>
    /// 使用手机号创建会员请求类
    /// </summary>
    public class CreateMemberWithPhoneNumberRequest
    {
        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// 短信验证码
        /// </summary>
        public string SmsCode { get; set; }
    }
}