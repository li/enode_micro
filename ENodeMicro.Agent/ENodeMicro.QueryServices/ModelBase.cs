using System;
using System.ComponentModel.DataAnnotations;

namespace ENodeMicro.QueryServices
{

    public class ModelBase
    {
        /// <summary>
        /// 聚合根ID
        /// </summary>
        [Required]
        [MaxLength(32)]
        public string Id { get; set; }
        /// <summary>
        /// 自增
        /// </summary>
        [Key]
        public int Sequence { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Required]
        public DateTime CreatedOn { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Required]
        public DateTime UpdatedOn { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        [Required]
        [MaxLength(32)]
        public int Version { get; set; }
    }
}