using System.Configuration;
using System.Reflection;
using ECommon.Components;
using ECommon.Configurations;
using ECommon.Logging;
using ECommon.Serilog;
using ENode.Configurations;
using ENode.Infrastructure;
using ENode.MySQL;
using ENodeMicro.Common;
using ENodeMicro.Domain.Managers;

namespace ENodeMicro.CommandHost
{
    public class Bootstrap
    {
        private static ENodeConfiguration _enodeConfiguration;

        public static void Initialize()
        {
            InitializeENode();
            InitializeCommandService();
        }
        public static void Start()
        {
            _enodeConfiguration.StartEQueue().Start();
        }
        public static void Stop()
        {
            _enodeConfiguration.ShutdownEQueue().Stop();
        }

        private static void InitializeENode()
        {
            var assemblies = new[]
            {
                Assembly.Load("ENodeMicro.CommandHandlers"),
                Assembly.Load("ENodeMicro.Commands"),
                Assembly.Load("ENodeMicro.Common"),
                Assembly.Load("ENodeMicro.Domain"),
                Assembly.Load("ENodeMicro.Domain.Dapper"),
                Assembly.Load("ENodeMicro.MessagePublishers"),
                Assembly.Load("ENodeMicro.Messages"),
                Assembly.Load("ENodeMicro.ProcessManagers"),
                Assembly.Load("ENodeMicro.CommandHost")
            };

            var loggerFactory = new SerilogLoggerFactory();
            // .AddFileLogger("ECommon", "logs\\ecommon")
            // .AddFileLogger("EQueue", "logs\\equeue")
            // .AddFileLogger("ENode", "logs\\enode", minimumLevel: Serilog.Events.LogEventLevel.Debug)
            // .AddFileLogger("ENode.EQueue", "logs\\enode.equeue", minimumLevel: Serilog.Events.LogEventLevel.Debug);

            _enodeConfiguration = ECommon.Configurations.Configuration
                .Create()
                .UseAutofac()
                .RegisterCommonComponents()
                .UseSerilog(loggerFactory)
                .UseJsonNet()
                .RegisterUnhandledExceptionHandler()
                .CreateENode()
                .RegisterENodeComponents()
                .RegisterBusinessComponents(assemblies)
                .UseMySqlEventStore()
                .UseMySqlLockService()
                .UseEQueue()
                .BuildContainer()
                .InitializeMySqlEventStore(ConfigSettings.ENodeConnectionString)
                .InitializeMySqlLockService(ConfigSettings.ENodeConnectionString)
                .InitializeBusinessAssemblies(assemblies);
        }
        private static void InitializeCommandService()
        {
            ObjectContainer.Resolve<ILockService>().AddLockKey(nameof(Manager));
            ObjectContainer.Resolve<ILoggerFactory>().Create(typeof(Program)).Info("Command Host initialized.");
        }
    }
}