using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using ENodeMicro.Common;
using ENodeMicro.QueryServices.Models.Members;

namespace ENodeMicro.QueryServices.Interfaces.Members
{
    /// <summary>
    /// 会员查询服务接口
    /// </summary>
    public interface IMemberQueryService
    {
        Task<APIResult<IList<Member>>> GetMembersAsync();
    }
}