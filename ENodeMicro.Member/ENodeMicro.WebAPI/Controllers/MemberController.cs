using System.Threading.Tasks;
using ECommon.Utilities;
using ENode.Commanding;
using ENodeMicro.Commands.Members;
using ENodeMicro.Common;
using ENodeMicro.Common.Helpers;
using ENodeMicro.WebAPI.Models.Members;
using Microsoft.AspNetCore.Mvc;

namespace ENodeMicro.WebAPI.Controllers
{
    /// <summary>
    /// 会员控制器
    /// </summary>
    public class MemberController : MemberControllerBase
    {
        private readonly ICommandService _commandService;

        public MemberController(ICommandService commandService)
        {
            _commandService = commandService;
        }

        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<APIResult<string>> Create([FromBody] CreateMemberWithPhoneNumberRequest request)
        {
            return await CommandHelper.CommandResultAsync(async () =>
            {
                var rsp = new APIResult<string>();

                if (request.SmsCode != "123456")
                {
                    rsp.Message = "验证码错误.";
                    return rsp;
                }

                // 执行创建命令
                var result = await _commandService.ExecuteAsync(new CreateMemberWithPhoneNumberCommand(
                        ObjectId.GenerateNewStringId(),
                        request.PhoneNumber
                    ), CommandReturnType.EventHandled);

                return GetCommandApiResult(result, rsp);
            });
        }
    }
}