using System.Threading.Tasks;
using ECommon.Components;
using ENode.Messaging;
using ENodeMicro.Common;
using ENodeMicro.Domain.Members.Events;

namespace ENodeMicro.Denormalizers.Dapper
{
    /// <summary>
    /// 会员
    /// </summary>
    [Component]
    public class MemberDenormalizer : AbstractDenormalizer,
        IMessageHandler<MemberCreatedEvent>
    {
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task HandleAsync(MemberCreatedEvent message)
        {
            await TryInsertRecordAsync(async connection => await connection.InsertAsync(new
            {
                Id = message.AggregateRootId,

                message.Nickname,
                message.Gender,
                message.PhoneNumber,
                message.MemberLevelInfo.MemberLevel,
                message.MemberLevelInfo.LastGradedTime,
                message.MemberLevelInfo.Experience,
                message.MemberLevelInfo.Integral,
                message.MemberLevelInfo.ReturnPoint,
                message.Status,
                message.AvatarUrl,
                message.MemberWeChatInfo.WeChatOpenId,
                message.MemberWeChatInfo.WeChatMpOpenId,
                message.MemberWeChatInfo.WeChatUnionId,

                message.MemberAlipayInfo.AlipayUserId,

                CreatedOn = message.Timestamp,
                UpdatedOn = message.Timestamp,
                message.Version
            }, TableNames.Member));
        }
    }
}