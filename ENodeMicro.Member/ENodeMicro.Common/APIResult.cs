using System;

namespace ENodeMicro.Common
{
    /// <summary>
    /// APIResult 接口统一返回类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class APIResult<T>
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// 状态码
        /// </summary>
        public int StatusCode { get; set; } = 200;
        /// <summary>
        /// 消息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 数据集
        /// </summary>
        public T Data { get; set; }
    }
}