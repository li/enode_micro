using System.ComponentModel;

namespace ENodeMicro.Common.Enums
{
    /// <summary>
    /// 性别
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// 保密
        /// </summary>
        [Description("保密")]
        Other = 0,
        /// <summary>
        /// 帅哥
        /// </summary>
        [Description("帅哥")]
        Male = 1,
        /// <summary>
        /// 美女
        /// </summary>
        [Description("美女")]
        Female = 2
    }

    /// <summary>
    /// 会员等级
    /// </summary>
    public enum MemberLevel
    {
        /// <summary>
        /// 奶茶爱好者
        /// </summary>
        [Description("奶茶爱好者")]
        V1 = 1,
        /// <summary>
        /// 奶茶学徒
        /// </summary>
        [Description("奶茶学徒")]
        V2 = 2,
        /// <summary>
        /// 奶茶师
        /// </summary>
        [Description("奶茶师")]
        V3 = 3,
        /// <summary>
        /// 奶茶大师
        /// </summary>
        [Description("奶茶大师")]
        V4 = 4,
        /// <summary>
        /// 顶级奶茶师
        /// </summary>
        [Description("顶级奶茶师")]
        V5 = 5,
    }
}