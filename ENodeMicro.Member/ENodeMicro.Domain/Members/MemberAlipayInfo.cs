namespace ENodeMicro.Domain.Members
{
    /// <summary>
    /// 会员支付宝相关信息
    /// </summary>
    public class MemberAlipayInfo
    {
        /// <summary>
        /// 支付宝UserID
        ///     预留
        /// </summary>
        public string AlipayUserId { get; private set; }

        public MemberAlipayInfo(string alipayUserId)
        {
            AlipayUserId = alipayUserId;
        }
    }
}