using System;
using ENode.Domain;
using ENodeMicro.Common.Enums;
using ENodeMicro.Domain.Members.Events;

namespace ENodeMicro.Domain.Members
{
    /// <summary>
    /// 会员聚合根
    /// </summary>
    public class Member : AggregateRoot<string>
    {
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; private set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; private set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string AvatarUrl { get; private set; }
        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; private set; }
        /// <summary>
        /// 会员状态
        /// </summary>
        public Status Status { get; private set; }
        /// <summary>
        /// 会员微信信息
        /// </summary>
        public MemberWeChatInfo MemberWeChatInfo { get; private set; }
        /// <summary>
        /// 会员支付宝信息
        /// </summary>
        public MemberAlipayInfo MemberAlipayInfo { get; private set; }
        /// <summary>
        /// 会员等级信息
        /// </summary>
        public MemberLevelInfo MemberLevelInfo { get; private set; }
        /// <summary>
        /// 会员基本信息
        /// </summary>
        public MemberBasicInfo MemberBasicInfo { get; private set; }
        private Member() { }

        /// <summary>
        /// 使用手机号创建
        /// </summary>
        /// <param name="id">聚合根ID</param>
        /// <param name="phoneNumber">手机号</param>
        public Member(string id, string phoneNumber)
            : base(id)
        {
            // 昵称
            var nickname = $"用户{DateTime.Now:HHmmss}";
            // 添加默认头像
            const string avatarUrl = "https://mmbiz.qpic.cn/mmbiz_png/3tk3yeywFT3vRE32E8ON9ACTqG5dJzWVGYtSkSsJdUDRJcsLndklbibGCPxwQic1DwgzicZdr7vYNMChibAFTa3gPQ/0?wx_fmt=png";
            // 初始化会员等级信息
            var memberLevelInfo = new MemberLevelInfo(MemberLevel.V1, DateTime.Now, 0, 0, 0);
            // 初始化微信信息
            var memberWeChatInfo = new MemberWeChatInfo("", "", "");
            // 初始化支付宝信息
            var memberAlipayInfo = new MemberAlipayInfo("");
            // 性别默认保密
            const Gender gender = Gender.Other;
            // 状态默认启用
            const Status status = Status.Enabled;
            // 发布事件
            ApplyEvent(new MemberCreatedEvent(nickname, phoneNumber, gender, avatarUrl, memberLevelInfo, memberWeChatInfo, memberAlipayInfo, status));
        }
        /// <summary>
        /// 使用小程序创建
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nickname"></param>
        /// <param name="avatarUrl"></param>
        /// <param name="gender"></param>
        /// <param name="weChatMpOpenId"></param>
        public Member(string id, string nickname, string avatarUrl, Gender gender, string weChatMpOpenId)
            : base(id)
        {
            // TODO
        }

        #region Private handles
        // 使用手机号注册
        private void Handle(MemberCreatedEvent @event)
        {
            Nickname = @event.Nickname;
            PhoneNumber = @event.PhoneNumber;
            Gender = @event.Gender;
            AvatarUrl = @event.AvatarUrl;
            MemberLevelInfo = @event.MemberLevelInfo;
            MemberWeChatInfo = @event.MemberWeChatInfo;
            MemberAlipayInfo = @event.MemberAlipayInfo;
            Status = @event.Status;
        }
        #endregion
    }
}