using System;
using ENodeMicro.Common.Enums;

namespace ENodeMicro.Domain.Members
{
    /// <summary>
    /// 会员等级信息
    /// </summary>
    public class MemberLevelInfo
    {
        /// <summary>
        /// 会员等级
        /// </summary>
        public MemberLevel MemberLevel { get; private set; }
        /// <summary>
        /// 会员最后一次晋级或保级日期
        /// </summary>
        public DateTime LastGradedTime { get; private set; }
        /// <summary>
        /// 经验值
        ///     通过消费获得
        /// </summary>
        public int Experience { get; private set; }
        /// <summary>
        /// 积分
        ///     通过消费获得
        /// </summary>
        public int Integral { get; private set; }
        /// <summary>
        /// 返点
        ///     通过他人点单DIY订单产生返点值
        /// </summary>
        public int ReturnPoint { get; private set; }

        public MemberLevelInfo(MemberLevel memberLevel, DateTime lastGradedTime, int experience, int integral, int returnPoint)
        {
            MemberLevel = memberLevel;
            LastGradedTime = lastGradedTime;
            Experience = experience;
            Integral = integral;
            ReturnPoint = returnPoint;
        }
    }
}