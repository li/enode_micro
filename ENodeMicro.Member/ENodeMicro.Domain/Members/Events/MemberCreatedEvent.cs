using ENode.Eventing;
using ENodeMicro.Common.Enums;

namespace ENodeMicro.Domain.Members.Events
{
    /// <summary>
    /// 会员创建成功事件
    ///     使用手机号创建
    /// </summary>
    public class MemberCreatedEvent : DomainEvent<string>
    {
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; private set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; private set; }
        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; private set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string AvatarUrl { get; private set; }
        /// <summary>
        /// 会员等级信息
        /// </summary>
        public MemberLevelInfo MemberLevelInfo { get; private set; }
        /// <summary>
        /// 会员微信信息
        /// </summary>
        public MemberWeChatInfo MemberWeChatInfo { get; private set; }
        /// <summary>
        /// 会员支付宝信息
        /// </summary>
        public MemberAlipayInfo MemberAlipayInfo { get; private set; }
        /// <summary>
        /// 会员状态
        /// </summary>
        public Status Status { get; private set; }
        private MemberCreatedEvent() { }

        public MemberCreatedEvent(
            string nickname,
            string phoneNumber,
            Gender gender,
            string avatarUrl,
            MemberLevelInfo memberLevelInfo,
            MemberWeChatInfo memberWeChatInfo,
            MemberAlipayInfo memberAlipayInfo,
            Status status)
        {
            Nickname = nickname;
            PhoneNumber = phoneNumber;
            Gender = gender;
            AvatarUrl = avatarUrl;
            MemberLevelInfo = memberLevelInfo;
            MemberWeChatInfo = memberWeChatInfo;
            MemberAlipayInfo = memberAlipayInfo;
            Status = status;
        }
    }
}